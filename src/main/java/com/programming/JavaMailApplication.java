package com.programming;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;

import javax.mail.MessagingException;

@SpringBootApplication
public class JavaMailApplication {

    @Autowired
    public MailSenderService mailSenderService;

    public static void main(String[] args) {
        SpringApplication.run(JavaMailApplication.class, args);
    }

    @EventListener(ApplicationReadyEvent.class)
    public void triggerMail() {

        //Mail Without Attachment
        MailWithoutAttachment mailWithoutAttachment = new MailWithoutAttachment("shubhamsri1991@gmail.com",
                "This is a sample mail subject",
                "This is a sample mail body");
        mailSenderService.sendMailWithoutAttachment(mailWithoutAttachment);

        //Mail With Attachment
        MailWithAttachment mailWithAttachment = new MailWithAttachment("shubhamsri1991@gmail.com",
                "This is a sample mail with attachment subject",
                "This is a sample mail with attachment body",
                "C:\\Users\\SHUBHAM\\Downloads\\form1.doc");
        try {
            mailSenderService.sendMailWithAttachment(mailWithAttachment);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

}
