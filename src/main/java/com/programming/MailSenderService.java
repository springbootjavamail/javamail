package com.programming;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;

@Service
public class MailSenderService {

    private final JavaMailSender javaMailSender;

    @Autowired
    public MailSenderService(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    public void sendMailWithoutAttachment(MailWithoutAttachment mailWithoutAttachment) {

        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();

        simpleMailMessage.setTo(mailWithoutAttachment.getToEmail());
        simpleMailMessage.setSubject(mailWithoutAttachment.getSubject());
        simpleMailMessage.setText(mailWithoutAttachment.getBody());

        javaMailSender.send(simpleMailMessage);

        System.out.println("Mail sent to : " + mailWithoutAttachment.getToEmail());

    }

    public void sendMailWithAttachment(MailWithAttachment mailWithAttachment) throws MessagingException {

        MimeMessage mimeMessage = javaMailSender.createMimeMessage();

        MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true);

        FileSystemResource fileSystemResource = new FileSystemResource(new File(mailWithAttachment.getAttachment()));

        mimeMessageHelper.setTo(mailWithAttachment.getToEmail());
        mimeMessageHelper.setSubject(mailWithAttachment.getSubject());
        mimeMessageHelper.setText(mailWithAttachment.getBody());
        mimeMessageHelper.addAttachment(fileSystemResource.getFilename(), fileSystemResource);

        javaMailSender.send(mimeMessage);

        System.out.println("Mail sent with attachment to : " + mailWithAttachment.getToEmail());

    }
}
